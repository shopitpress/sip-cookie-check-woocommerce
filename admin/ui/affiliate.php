


<div class="alert alert-info mg-b-0" role="alert">
            <h5 class="card-heading">Be awesome</h5>
            <hr>
            <p>Do you like this plugin? Would you like to see even more great features? Please be awesome and help us maintain and develop free plugins by checking the option below</p>
            <hr>
            <p class="mb-0">	
	<form method="post" action="options.php">
	  <?php settings_fields( 'sip-rswc-affiliate-settings-group' ); ?>
	  <?php $options = get_option('sip-rswc-affiliate-radio'); ?>
		
			<div class="form-group">
    <div class="custom-control custom-checkbox">
	<input class="custom-control-input" id="spc-rswc-affiliate-checkbox" type="checkbox" name="sip-rswc-affiliate-check-box" value="true" <?php echo esc_attr( get_option('sip-rswc-affiliate-check-box', false))?' checked="checked"':''; ?> />
      
      <label class="custom-control-label" for="spc-rswc-affiliate-checkbox">Yes, I want to help development of this plugin</label>
    </div>
  </div>
			
			
			
			
			<div id="spc-rswc-diplay-affiliate-toggle">
<div class="form-group">
<div class="custom-control custom-radio">
<input class="custom-control-input" id="spc-rswc-discreet-credit" type="radio" name="sip-rswc-affiliate-radio[option_three]" value="value1"<?php checked( 'value1' == $options['option_three'] ); ?>/>
  <label class="custom-control-label" for="spc-rswc-discreet-credit">Checked / Add a discreet credit</label>
</div>
</div>

		<div class="form-group">		
<div class="custom-control custom-radio">
 <input class="custom-control-input" id="spc-rswc-affiliate-link" 	type="radio" name="sip-rswc-affiliate-radio[option_three]" value="value2"<?php checked( 'value2' == $options['option_three'] ); ?> />
  <label class="custom-control-label" for="spc-rswc-affiliate-link">Add my affiliate link</label>
</div>
</div>
				<div id="spc-rswc-affiliate-link-box">
				<div class="form-group">
				
    <label for="sip-rswc-affiliate-affiliate-username" class="d-block"> Input affiliate username/ID</label>
    <input type="text" class="form-control" name="sip-rswc-affiliate-affiliate-username" value="<?php echo esc_attr( get_option('sip-rswc-affiliate-affiliate-username')) ?>" />
  </div>
  </div>
            <p class="sip-text">Make money recommending our plugins. Register for an affiliate account at <a class="sip-link" href="https://shopitpress.com/affiliate-area/?utm_source=wordpress.org&amp;utm_medium=affiliate&amp;utm_campaign=sip-reviews-shortcode-woocommerce" target="_blank">Shopitpress</a>.
            </p>
					</a>
			</div>
			
		<?php //submit_button(); ?>
	</form></p>
          </div>
		  
		  
		  

<script type="text/javascript">
	jQuery(document).ready(function(){

		jQuery("#spc-rswc-diplay-affiliate-toggle").hide();
		jQuery("#spc-rswc-affiliate-link-box").hide();

		if (jQuery('#spc-rswc-affiliate-checkbox').is(":checked"))
		{
		  jQuery("#spc-rswc-diplay-affiliate-toggle").show('slow');
		}

		jQuery('#spc-rswc-affiliate-checkbox').click(function() {
		  jQuery('#spc-rswc-diplay-affiliate-toggle').toggle('slow');
		})

		if (jQuery('#spc-rswc-affiliate-link').is(":checked"))
		{
		  jQuery("#spc-rswc-affiliate-link-box").show('slow');
		}

		jQuery('#spc-rswc-affiliate-link').click(function() {
		  jQuery('#spc-rswc-affiliate-link-box').show('slow');
		})

		jQuery('#spc-rswc-discreet-credit').click(function() {
		  jQuery('#spc-rswc-affiliate-link-box').hide('slow');
		})

	});
</script>
