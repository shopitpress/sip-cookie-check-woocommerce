<div class="card ht-100p">
<div class="card-body">
 <p>To enable the plugin simply check the option "Enable in WooCommerce pages" and save. You can also display the notice anywhere with the shortcode [sip-cookie-check] even if the plugin is not enabled in WooCommerce pages.</p>
</div>


<div class="card-footer text-center">

<div class="sip-versionm">
  <?php $get_optio_version = get_option( 'sip_version_value' ); echo "SIP Version " . $get_optio_version; ?>
</div> 
<!-- .sip-version -->
</div>
  
  </div>




