<div class="card ">
 
	<form method="post" action="options.php">
    <?php settings_fields( 'sip-ccwc-settings-group' ); ?>
	
	
	
	 <div class="card-body">
	 <div class="row">
	  <div class="col-md-4">
  <div class="form-group">
   
   <div class="custom-control custom-checkbox">
     <input class="custom-control-input" type="checkbox" id="sip-ccwc-display-text-editor-checkbox" name="display_sip_ccwc_message" value="true" <?php echo esc_attr( get_option('display_sip_ccwc_message', false))?' checked="checked"':''; ?> /> 
      <label class="custom-control-label" for="sip-ccwc-display-text-editor-checkbox">Enable in WooCommerce pages</label>
    </div>
   
  </div>
  </div>
   <div class="col-md-4">
   <div class="form-group">
  
  
   <div class="custom-control custom-checkbox">
    <input class="custom-control-input" id="sip_ccwc_css" type="checkbox" name="sip_ccwc_css_enable_desable" value="true" <?php echo esc_attr( get_option('sip_ccwc_css_enable_desable', false))?' checked="checked"':''; ?> />
      <label class="custom-control-label" for="sip_ccwc_css">Disable styling (CSS)</label>
    </div>
	
   </div>
   </div>
   <div class="col-md-4">
   <div class="form-group">
     <div class="custom-control custom-checkbox">
    <input class="custom-control-input" type="checkbox" id="sip-ccwc-customise-message-checkbox" name="sip_ccwc_customise_message_checkbox" value="true" <?php echo esc_attr( get_option('sip_ccwc_customise_message_checkbox', false))?' checked="checked"':''; ?> />
      <label class="custom-control-label" for="sip-ccwc-customise-message-checkbox">Customise message</label>
    </div>
	
	
   
  
   </div>
    </div>
	
	<div class="col-md-12">
	 <div class="clear"></div>
     <div id="sip-ccwc-display-text-editor">
            <?php
              if( ( get_option('sip_ccwc_message_editor') == "" ) || ( get_option('sip_ccwc_customise_message_checkbox', false) == false) ){
                $editor_content = '<strong>Cookies are disabled in your browser, you need to enable cookies to make purchases in this store</strong> - Please enable cookies. Learn how to do it by <a href="https://shopitpress.com/enable-cookies/" target="_blank">clicking here</a>. ';
                update_option( 'sip_ccwc_message_editor', $editor_content );
              }

              $settings       = array('teeny' => false, 'tinymce' => true, 'textarea_rows' => 12, 'tabindex' => 1 );
              $editor_id      = "sip_ccwc_message_editor";
              $editor_content = get_option('sip_ccwc_message_editor');
              wp_editor( $editor_content, $editor_id, $settings );
            ?>
          </div>
		  </div>
		  
		  
   </div>
   
   </div>
	 <div class="card-footer text-center"><input type="submit" name="submit" id="submit" class="btn btn-danger" value="Update"  /></div>
	 
    <?php //submit_button(); ?>
	</form>

</div>
<script type="text/javascript">
  jQuery(document).ready(function(){

    jQuery("#sip-ccwc-display-text-editor").hide();

    if (jQuery('#sip-ccwc-customise-message-checkbox').is(":checked"))
    {
      jQuery("#sip-ccwc-display-text-editor").show('slow');
    }

    jQuery('#sip-ccwc-customise-message-checkbox').click(function() {
      jQuery('#sip-ccwc-display-text-editor').toggle('slow');
    })

  });
</script>
